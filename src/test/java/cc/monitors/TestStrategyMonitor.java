package cc.monitors;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cc.strategy.Strategy;

public class TestStrategyMonitor {
	
	public List<Double> startParam;
	public List<Double> tradeParam;
	public double stoppingThreshold;
	@Before
	public void init() {
		startParam = new LinkedList<Double>();
		tradeParam = new LinkedList<Double>();
		
		startParam.add(1.0);
		startParam.add(2.0);
		startParam.add(3.0);
		
		tradeParam.add(1.0);
		tradeParam.add(2.0);
		tradeParam.add(3.0);
		
		stoppingThreshold = 0.5;
	}
	
	@Test
	public void testCreateStrategy() {
		StrategyMonitor strategyMon = new StrategyMonitor();
		//Create a strategy
		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, tradeParam, stoppingThreshold);
		assertEquals(1, strategyMon.getAllStrategies().size());
		assertEquals("100", strategyMon.getStrategy("100").getId());
		strategyMon.removeAllStrategy();
		assertEquals(0, strategyMon.getAllStrategies().size());
	}
	
	@Test
	public void testRemoveStrategy() {
		
		StrategyMonitor strategyMon = new StrategyMonitor();
		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("200", "GOOG", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.removeStrategy("100");
		assertEquals(strategyMon.getStrategy("100"), null);
	}
	
	@Test
	public void testRemoveAllStrategy() {
		
		StrategyMonitor strategyMon = new StrategyMonitor();
		//Create strategies
		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("200", "GOOG", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("300", "WMT", "TwoMA", startParam, tradeParam, stoppingThreshold);
		//Remove all strategies
		strategyMon.removeAllStrategy();
		assertEquals(0, strategyMon.getAllStrategies().size());
	}
	
	
	@Test
	public void testStartandPauseStrategy() throws Exception {
		StrategyMonitor strategyMon = new StrategyMonitor();
		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("200", "GOOG", "TwoMA", startParam, tradeParam, stoppingThreshold);
		//Start two strategies
		strategyMon.startStrategy("100");
		strategyMon.startStrategy("200");
		assertEquals(2, strategyMon.getRunningStrategies().size());
		//Pause one running strategy
		strategyMon.pauseStrategy("100");
		assertEquals(1, strategyMon.getRunningStrategies().size());
		assertEquals(1, strategyMon.getPausedStrategies().size());
	}
	
	@Test
	public void testStartandStopAllStrategy() throws Exception {
		StrategyMonitor strategyMon = new StrategyMonitor();
		strategyMon.createStrategy("100", "AAPL", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("200", "GOOG", "TwoMA", startParam, tradeParam, stoppingThreshold);
		strategyMon.createStrategy("300", "WMT", "TwoMA", startParam, tradeParam, stoppingThreshold);
		
//		for(Strategy s : strategyMon.getAllStrategies()) {
//			System.out.println(s.getId());
//		}
		//Start all strategy
		strategyMon.startAllStrategy();
		assertEquals(3, strategyMon.getRunningStrategies().size());
		//Stop all strategy
		strategyMon.stopAllStrategy();
		assertEquals(0, strategyMon.getRunningStrategies().size());
		
	}

	
	
	
}
