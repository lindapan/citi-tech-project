package cc.monitors;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cc.listener.MarketDataHandler;

public class TestStockMonitor {
	public String stock1;
	public String stock2;
	public String stock3;
	public int period1;
	public int period2;
	public int period3;
	public MarketDataHandler mdh;
	public StockMonitor sm;
	
	@Before
	public void init() {
		stock1 = "MSFT";
		stock2 = "APPL";
		stock3 = "GOOG";
		
		period1 = 20;
		period2 = 30;
		period3 = 300;
		mdh = MarketDataHandler.getInstance();
		
	}
	
	@Test
	public void testAddStock() {
		sm = new StockMonitor();
		sm.addStock(stock1);
		sm.addStock(stock2);
		assertEquals(2, sm.getNumberOfStocks());
		sm.addStock(stock3);
		assertEquals(3, sm.getNumberOfStocks());
		
	}
	
	@Test
	public void testRemoveStock() {
		sm = new StockMonitor();
		System.out.println(sm.getNumberOfStocks());
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.removeStock(stock1);
		assertEquals(1, sm.getNumberOfStocks());
		sm.addStock(stock3);
		assertEquals(2, sm.getNumberOfStocks());
	}
	
	@Test
	public void testRemoveAllStock() {
		sm = new StockMonitor();
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.addStock(stock3);
		assertEquals(3, sm.getNumberOfStocks());
		sm.removeAllStocks();
		assertEquals(0, sm.getNumberOfStocks());
	}
	
	@Test
	public void testGetStock() {
		sm = new StockMonitor();
		sm.addStock(stock1);
		sm.addStock(stock2);
		sm.addStock(stock3);
		assertEquals("MSFT", sm.getStock(stock1).getListing());
	}
	
	@Test
	public void testGetPeriod() {
		sm = new StockMonitor();
		sm.addStock(stock1, period1);
		sm.addStock(stock2, period2);
		sm.addStock(stock3, period3);
		assertEquals(period1, sm.getStock(stock1).getHighPrice().size());
		assertEquals(period2, sm.getStock(stock2).getLowPrice().size());
		assertEquals(period2, sm.getStock(stock2).getCurrPrice().size());
	}
	
}
