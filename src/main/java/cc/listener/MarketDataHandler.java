package cc.listener;

import java.awt.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import cc.stock.Stock;

public class MarketDataHandler extends Observable {
	private static final MarketDataHandler instance = new MarketDataHandler();
	private static int period = 120;
	private MarketDataHandler() {
	}

	public static MarketDataHandler getInstance() {
		return instance;
	}

	/**
	 * @param stockName
	 * @param period
	 * @return
	 * @throws Exception
	 */
	public static Stock getStockInfoInRange(String stockName, int period) throws Exception {
		LinkedList<Double> highPrices = new LinkedList<Double>();
		LinkedList<Double> lowPrices = new LinkedList<Double>();
		LinkedList<Double> open = new LinkedList<Double>();
		LinkedList<Double> close = new LinkedList<Double>();
		LinkedList<Integer> volumes = new LinkedList<Integer>();
		LinkedList<Timestamp> timestamps = new LinkedList<Timestamp>();
		Stock queryStock = null;
		
		StringBuilder result = new StringBuilder();
		String pURL = "http://incanada1.conygre.com:9080/prices/" + stockName + "?periods=" + period;
		URL url = new URL(pURL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

		while ((line = rd.readLine()) != null) {
			result.append(line);
			String[] priceLine = line.split(",");
			try {
				Date parsedDate = dateFormat.parse(priceLine[0]);
				Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

				timestamps.add(timestamp);
				open.add(Double.parseDouble(priceLine[1]));
				highPrices.add(Double.parseDouble(priceLine[2]));
				lowPrices.add(Double.parseDouble(priceLine[3]));
				close.add(Double.parseDouble(priceLine[4]));
				volumes.add(Integer.parseInt(priceLine[5]));
			} catch (Exception e) {
			}
			
//			for (String s : priceLine) {
//				System.out.print(s + " ");
//			}
//			System.out.println("");
		}
		rd.close();
		queryStock = new Stock(stockName, highPrices, lowPrices, open, close, volumes, timestamps);
		return queryStock;
	}

	public static Stock getStock(String stockName, int p) throws Exception {
		period = p;
		return getStockInfoInRange(stockName, period);
	}

	public static Stock getStock(String stockName) throws Exception {
		return getStockInfoInRange(stockName, period);
	}
	
//	public static void main(String[] args) {
//		try {
//			Stock microsoft = getStock("MSFT", 120);
//			Stock apple = getStock("APPL", 5);
////			microsoft.toString();
//			apple.toString();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
