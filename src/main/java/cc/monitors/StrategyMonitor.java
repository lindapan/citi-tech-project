package cc.monitors;

import java.util.LinkedList;
import java.util.List;

import cc.strategy.Strategy;
import cc.strategy.Strategy.Status;

//TODO Rewrite it to a singleton
public class StrategyMonitor {
	private List<Strategy> strategies;
	private List<Strategy> runningStrategies;
	private List<Strategy> pausedStrategies;

	public StrategyMonitor() {
		
		strategies = new LinkedList<Strategy>();
		
		runningStrategies = new LinkedList<Strategy>();
		for (Strategy s : strategies) {
			if (s.getStatus().equals(Status.RUNNING)) {
				runningStrategies.add(s);
			}
		}

		pausedStrategies = new LinkedList<Strategy>();
		for (Strategy s : strategies) {
			if (s.getStatus().equals(Status.STOPPED)) {
				pausedStrategies.add(s);
			}
		}
		

		// TODO: write to database
	}

	public void createStrategy(String id, String stockName, String strategyTypeString, List<Double> stratParameters,
			List<Double> tradeParameters, double stoppingThreshold) {
		
		try {
			if (getStrategy(id) == null) {
				
				Strategy newStrategy = new Strategy(id, strategyTypeString, stockName, stratParameters,
						tradeParameters, stoppingThreshold);

				newStrategy.start();

				this.strategies.add(newStrategy);
				this.runningStrategies.add(newStrategy);
				
				// TODO: write to database
			} else {
				System.out.println(getStrategy(id).getId());
				// throw some exception
				System.out.println("Strategy with same ID already running.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeStrategy(String id) {
		try {
			Strategy foundStrategy = getStrategy(id);
			strategies.remove(foundStrategy);
			if (foundStrategy.getStatus().equals(Status.RUNNING)) {
				runningStrategies.remove(foundStrategy);
			}

			if (foundStrategy.getStatus().equals(Status.STOPPED)) {
				runningStrategies.remove(foundStrategy);
			}
		} catch (Exception e) {
			// Strategy not in monitor
		}
	}

	public void removeAllStrategy() {
		strategies.clear();
		runningStrategies.clear();
		pausedStrategies.clear();
	}

	public List<Strategy> getAllStrategies() {
		return strategies;
	}

	public List<Strategy> getRunningStrategies() {

		return runningStrategies;
	}

	public List<Strategy> getPausedStrategies() {

		return pausedStrategies;
	}

	public void startStrategy(String id) throws Exception {
		Strategy s = null;
		if ((s = getStrategy(id)) != null && s.getStatus().equals(Status.STOPPED)) {
			runningStrategies.add(s);
			pausedStrategies.remove(s);
			s.start();
		}
	}

	public void pauseStrategy(String id) {
		Strategy s = null;
		if ((s = getStrategy(id)) != null && s.getStatus().equals(Status.RUNNING)) {
			runningStrategies.remove(s);
			pausedStrategies.add(s);
			s.pause();
		}
	}

	public Strategy getStrategy(String id) {
		Strategy foundStrategy = null;
		for (Strategy s : strategies) {
			if (s.getId().equals(id)) {
				foundStrategy = s;
			}
		}
		return foundStrategy;
	}

	public void stopAllStrategy() {
		for (Strategy s : strategies) {
			s.pause();
			for (Strategy s1 : runningStrategies) {
				pausedStrategies.add(s1);
			}
			runningStrategies.clear();
		}
	}

	public void startAllStrategy() throws Exception {
		for (Strategy s : strategies) {
			s.start();
			for (Strategy s1 : pausedStrategies) {
				runningStrategies.add(s1);
			}
			pausedStrategies.clear();
		}
	}
}
