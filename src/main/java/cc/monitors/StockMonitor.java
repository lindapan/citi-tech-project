package cc.monitors;

import java.util.HashMap;
import cc.stock.Stock;
import cc.listener.MarketDataHandler;

public class StockMonitor {
//	private static final StockMonitor instance = new StockMonitor();
	private HashMap<String, Stock> stocks;

	public StockMonitor() {
		stocks = new HashMap<String, Stock>();
	}

//	public StockMonitor getInstance() {
//		return instance;
//	}

	public Stock getStock(String stockName) {
		Stock target = null;
		try {
			target = stocks.get(stockName);
		} catch (Exception e) {
			target = null;
		}
		return target;
	}

	public HashMap<String, Stock> getAllStocks() {
		return stocks;
	}

	// This method should connect to front end
	public void addStock(String stockName, int period) {
		Stock newStock;
		try {
			newStock = MarketDataHandler.getStock(stockName, period);
			stocks.put(newStock.getListing(), newStock);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addStock(String stockName) {
		Stock newStock;
		try {
			newStock = MarketDataHandler.getStock(stockName);
			stocks.put(newStock.getListing(), newStock);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeStock(String stockName) {
		try {
			stocks.remove(stockName);
		} catch (Exception e) {
			System.out.println("No stock to be removed");
		}
	}

	public int getNumberOfStocks() {
		return stocks.size();
	}

	public void removeAllStocks() {
		stocks.clear();
	}
}
