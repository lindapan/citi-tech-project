package cc.stock;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

public class Stock {
	private String listing;
	private List<Double> highPrice;
	private List<Double> lowPrice;
	private List<Double> currPrice;
	private List<Double> open;
	private List<Double> close;
	private List<Integer> volume;
	private List<Timestamp> timeStamp;

	public Stock() {

	}

	public Stock(String name, List<Double> highPrice, List<Double> lowPrice, List<Double> open, List<Double> close,
			List<Integer> volume, List<Timestamp> timeStamp) {
		listing = name;
		this.highPrice = highPrice;
		this.lowPrice = lowPrice;
		this.open = open;
		this.close = close;
		this.volume = volume;
		this.timeStamp = timeStamp;
		this.currPrice = avg(highPrice, lowPrice);
	}

	public List<Double> avg(List<Double> l1, List<Double> l2) {
		List<Double> l3 = new LinkedList<Double>();
		try {
			for (int i = 0; i < l1.size(); i++) {
				l3.add((l1.get(i)+l2.get(i))/2.0);
			}
		} catch (Exception e) {

		}
		return l3;

	}
	
	public List<Double> getCurrPrice(){
		return this.currPrice;
	}
	
	public String getListing() {
		return listing;
	}

	public List<Double> getHighPrice() {
		return highPrice;
	}

	public List<Double> getLowPrice() {
		return lowPrice;
	}

	public List<Double> getOpen() {
		return open;
	}

	public List<Double> getClose() {
		return close;
	}

	public List<Integer> getVolume() {
		return volume;
	}

	public List<Timestamp> getTimeStamp() {
		return timeStamp;
	}

	public String toString() {
		System.out.println("Stock name:" + this.listing);
		System.out.println("Current window size:" + this.highPrice.size());
		System.out.println("Current high prices:");
		for (Double p : highPrice) {
			System.out.println(p);
		}
		return "";
	}

}
