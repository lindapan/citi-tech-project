package cc.run;

import cc.stock.Stock;
import cc.strategy.Strategy;
import cc.strategy.Strategy.StrategyType;

import java.sql.Timestamp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author jackh
 *
 */
public class DemoDriver {
	private EntityManagerFactory factory;
	private EntityManager em;

	private DemoDriver() {
		factory = Persistence.createEntityManagerFactory("oracle");
		// factory = Persistence.createEntityManagerFactory("oracle");
		em = factory.createEntityManager();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DemoDriver dd = new DemoDriver();
		dd.run();
	}

	private void run() {
/*		String sName = "BRAH";
		double price = 1.1;
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		
		String strategyID = "strategy3";
		StrategyType strategyType = StrategyType.TwoMA;
		
		Strategy strat1 = new Strategy(strategyID, strategyType);
		
		Stock s = new Stock(sName, price, ts);
		Stock s2 = new Stock();
		Stock findStock;
		
		em.getTransaction().begin();
		em.persist(s);
		em.persist(strat1);
		em.persist(s2);
		findStock = em.find(Stock.class, "APPL");
		em.getTransaction().commit();
		
		if(findStock != null) {
			System.out.println(findStock.getPrice());
		}
		
		

		factory.close();*/

	}

}
