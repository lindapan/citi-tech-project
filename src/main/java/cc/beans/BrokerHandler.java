package cc.beans;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.awt.List;
import java.sql.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import cc.model.Model.Decision;
import cc.strategy.Strategy.StrategyType;
import cc.strategy.Trade;

public class BrokerHandler {
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	public BrokerHandler() {
		context = new ClassPathXmlApplicationContext("spring-beans.xml");
		destination = context.getBean("destination", Destination.class);
		replyTo = context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}

//
//	public static void main(String[] args) {
//		BrokerHandler mmh = new BrokerHandler();
//		Trade t = null;
//		try {
//			ArrayList<Double> d = new ArrayList<Double>();
//			d.add(22.0);
//			d.add(67.0);
//			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//			t = new Trade("7", StrategyType.TwoMA, "AAP", d, 
//					Decision.BUY, new Timestamp(dateFormat.parse("2018-07-31 11:33:22.801-04:00").getTime()));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		mmh.makeTrade(t);
//	}

	public void makeTrade(Trade trade) {
		makeMessage(destination, trade);
		System.out.println("Message Sent to JMS Queue:- ");

	}

	private void makeMessage(final Destination destination, Trade trade) {
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				String xml = "<trade>"
						+ "<buy>"+trade.isBuy()+"</buy>"
						+ "<id>"+222+"</id>"
						+ "<price>"+trade.getPrice()+"</price>"
						+ "<size>"+trade.getAmount()+"</size>"
						+ "<stock>"+trade.getStockName()+"</stock>"
						+ "<whenAsDate>"+trade.getTimestamp()+"</whenAsDate>"
					+ "</trade>";
						
				System.out.println("Sending: "+xml);
				//String xml = "<trade> <buy>true</buy><id>2</id><price>99.0</price><size>2007</size><stock>AAPL</stock><whenAsDate>2018-07-31T11:33:22.801-04:00</whenAsDate></trade>";
				TextMessage message = session.createTextMessage(xml);
				message.setJMSReplyTo(replyTo);
				message.setJMSCorrelationID(trade.getId());
				return message;
			}
		});

	}

}
