package cc.beans;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Enumeration;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.swing.plaf.synth.SynthSplitPaneUI;

import org.jasypt.encryption.pbe.PooledPBEBigIntegerEncryptor;

import cc.monitors.StrategyMonitor;
import cc.strategy.Strategy;
import cc.strategy.Trade;

public class BrokerListener implements MessageListener{

	@Override
	public void onMessage(Message m) {
		TextMessage message = (TextMessage) m;
		try {
//			Enumeration<String> myEnum = (Enumeration<String>) message.getMapNames();
//			String s = null;
//			while(myEnum.hasMoreElements()) {
//				s += myEnum.nextElement();
//				 System.out.println(s);
//			}
			// Extract the id of the strategy used to make this trade
			String responseXml = message.getText();
			Trade tradeResponse = new Trade(responseXml, m.getJMSCorrelationID());
			String strategyId = tradeResponse.getStrategyId();
			String err = "";
			System.out.println(responseXml);
			try {
				// Find the stratgy object that called this trade
				StrategyMonitor sm = new StrategyMonitor();
				Strategy s = sm.getStrategy(strategyId);
				s.putExecutedTrade(tradeResponse);	
			}catch(NullPointerException e) {
				err+="Cannot find strategy "+strategyId+" for trade with ID "+tradeResponse.getId();
			}
			
			
			// Log the response from the server
			BufferedWriter writer = new BufferedWriter(new FileWriter("BrokerResponseLog.txt"));   
			writer.write(responseXml+"\r\n");     
			writer.write(err);
			writer.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}

	}
}
