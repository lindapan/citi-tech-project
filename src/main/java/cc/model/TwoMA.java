package cc.model;

import java.util.LinkedList;
import java.util.List;

import cc.strategy.Strategy.StrategyType;

public class TwoMA implements Model {

	private double longWindow;
	private double shortWindow;

	private enum flag {
		INIT, SHORT, LONG
	}
	//TODO: figure out how to keep track of flag
	private flag type = flag.INIT;

	public Decision getDecision(List<Double> parameters, LinkedList<Double> prices) {
		// TODO Auto-generated method stub
		try {
			parseParameters(parameters);
			Decision decision = null;
			double shortAverage = calcMA(shortWindow, prices);
			double longAverage = calcMA(longWindow, prices);

			if (shortAverage < 0 || longAverage < 0) {

				decision = Decision.DO_NOTHING;
			}

			if (type.equals(flag.INIT)) {
				if (shortAverage > longAverage) {
					type = flag.SHORT;
					decision = Decision.BUY;
				} else {
					type = flag.LONG;
					decision = Decision.SELL;
				}
			} else {
				if (longAverage > shortAverage) {
					if (type.equals(flag.LONG)) {
						decision = Decision.DO_NOTHING;
					} else {
						type = flag.LONG;
						decision = Decision.SELL;
					}

				} else {
					if (type.equals(flag.SHORT)) {
						decision = Decision.DO_NOTHING;

					} else {
						type = flag.SHORT;
						decision = Decision.BUY;
					}
				}

			}
			return decision;
		} catch (Exception e) {
			return null;
		}

	}

	public StrategyType getName() {
		return StrategyType.TwoMA;
	}
	
	public void setFlag(flag f) {
		this.type = f; 
		
	}
	private void parseParameters(List<Double> parameters) throws Exception {
		if (parameters.size() != 2) {
			throw new Exception("Bad parameters format");
		}
		try {
			this.longWindow = parameters.get(0);
			this.shortWindow = parameters.get(1);
		} catch (Exception e) {

		}
	}

	private double calcMA(double period, LinkedList<Double> dataStorage) {
		try {
			Double count = (double) 0;
			for (int i = 0; i < period; i++) {
				count += ((LinkedList<Double>) dataStorage).get(i);
			}
			return Math.round((count / period) * 10.0) / 10.0;
		} catch (Exception e) {
			System.out.println("Average has been asked too soon");
			return -1;
		}
	}
}
