package cc.model;

import java.util.LinkedList;
import java.util.List;

import cc.strategy.Strategy.StrategyType;

public interface Model {
	public enum Decision {
		BUY, SELL, DO_NOTHING
	}
	
	public Decision getDecision(List<Double> strategyParameters, LinkedList<Double> prices);
	
	public StrategyType getName();

}
