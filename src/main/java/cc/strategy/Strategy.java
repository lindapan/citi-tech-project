package cc.strategy;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Transient;

import cc.model.BollingerBands;
import cc.model.Model;
import cc.model.Model.Decision;
import cc.model.PriceBreakout;
import cc.model.TwoMA;
import cc.stock.Stock;
import cc.strategy.Trade.brokerResponse;
import cc.beans.BrokerHandler;
import cc.listener.MarketDataHandler;

@Entity(name = "Strategy")
@Table(name = "Strategy")
public class Strategy {
	@Id
	@Column(name = "Strategy ID", unique = true)
	@GeneratedValue
	private String id;

	public enum StrategyType {
		TwoMA, BollingerBands, PriceBreakout
	}

	public enum Status {
		RUNNING, STOPPED
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "Type", length = 20)
	private StrategyType type;

	@Enumerated(EnumType.STRING)
	@Column(name = "Current Status", length = 10)
	private Status status;

	@Column(name = "Stock Name")
	private String stockName;

	@Column(name = "Stop Threshold")
	private double stoppingThreshold;

	@Transient
	private List<Double> strategyParameters;

	@Transient
	private List<Double> tradeParameters;

	@Transient
	private HashMap<String, Trade> trades;

	@Transient
	private LinkedList<Double> prices;

	@Transient
	private Model model;

	@Transient
	private Queue<Trade> executedTrades;

	@Transient
	private boolean run;

	private double costToBuy;
	private double gainOfSell;
	private double profitOrLoss;
	private double roi;
	private EntityManagerFactory factory;
	private EntityManager em;
	private BrokerHandler bh;
	public Strategy() {
	}

	public Strategy(String id, String type, String stockName, List<Double> stratParameters,
			List<Double> tradeParameters, double stoppingThreshold) throws Exception {
		this.id = id;
		this.stockName = stockName;

		// TODO: get stock prices from marketDataCaller based on stockName
		Stock currStock = MarketDataHandler.getStock(stockName, 120);
		this.prices = (LinkedList<Double>) currStock.getHighPrice();

		this.strategyParameters = (LinkedList<Double>) stratParameters;
		this.tradeParameters = (LinkedList<Double>) tradeParameters;

		this.status = Status.STOPPED;
		this.stoppingThreshold = stoppingThreshold;

		switch (type) {
		case "TwoMA":
			this.type = StrategyType.TwoMA;
			this.model = new TwoMA();
			break;
		case "BollingerBands":
			this.type = StrategyType.BollingerBands;
			this.model = new BollingerBands();
			break;
		case "PriceBreakout":
			this.type = StrategyType.PriceBreakout;
			this.model = new PriceBreakout();
			break;
		default:
			this.type = null;
			this.model = null;
			break;
		}

		this.executedTrades = new LinkedList<Trade>();
		this.costToBuy = 0.0;
		this.gainOfSell = 0.0;
		this.profitOrLoss = 0.0;
		this.roi = 0.0;
		this.bh = new BrokerHandler();
		
		
		this.factory = Persistence.createEntityManagerFactory("oracle");
		// factory = Persistence.createEntityManagerFactory("oracle");
		this.em = factory.createEntityManager();
	}

	public Decision getDecision() {
		return this.model.getDecision(this.strategyParameters, this.prices);
	}

	public void writeToDatabase(Trade trade) {
		em.getTransaction().begin();
		em.persist(trade);
		em.getTransaction().commit();

	}

	// start the current strategy
	public void start() throws Exception {
		this.status = Status.RUNNING;

		// TODO: add exception for no strategy type found
		if (this.type == null) {

		} else {
			boolean isBelowThreshold = true;
			int tag = 1;
			Timestamp currTimestamp = new Timestamp(System.currentTimeMillis());
			Decision decision;

			// TODO: implement a listener here for marketCaller rather than while
			while (isBelowThreshold || run) {

				// TODO: Get input from user if he would like to stop

				Stock updatedStock = MarketDataHandler.getStock(this.stockName, 120);
				this.prices = (LinkedList<Double>) updatedStock.getCurrPrice();

				decision = getDecision();
				if (decision.equals(Decision.BUY) || decision.equals(Decision.SELL)) {
					String tradeId = this.id + "_" + this.stockName + "_" + tag;
					Trade trade = new Trade(tradeId, this.type, this.stockName, this.tradeParameters, decision,
							currTimestamp);
					trades.put(tradeId, trade);
					bh.makeTrade(trade);

					tag++;
				}

				if (!executedTrades.isEmpty()) {
					LinkedList<Double> performances = (LinkedList<Double>) evaluatePerformance();
					double profitOrLoss = performances.get(0);
					double roi = performances.get(1);
					isBelowThreshold = isBelowThreshold(profitOrLoss, roi);
				}
			}

			Thread.sleep(15000);
		}

	}

	public void putExecutedTrade(Trade trade) {
		this.executedTrades.add(trade);
	}

	public List<Double> evaluatePerformance() {
		LinkedList<Double> performances = new LinkedList<Double>();
		for (Trade executedTrade : executedTrades) {
			brokerResponse tradeStatus = executedTrade.getResponse();
			if (tradeStatus.equals(brokerResponse.FILLED) || tradeStatus.equals(brokerResponse.PARTIAL_FILLED)) {
				if (executedTrade.isBuy()) {
					this.costToBuy += executedTrade.getAmount() * executedTrade.getPrice();
				} else {
					this.gainOfSell += executedTrade.getAmount() * executedTrade.getPrice();
				}
			}
			this.profitOrLoss = this.gainOfSell - this.costToBuy;
			this.roi = (this.gainOfSell - this.costToBuy) / this.costToBuy;
			executedTrade.setProfitOrLoss(this.profitOrLoss);
			executedTrade.setROI(this.roi);
			// TODO write to database
			writeToDatabase(executedTrade);
		}

		this.executedTrades.clear();
		performances.add(profitOrLoss);
		performances.add(roi);
		
		return performances;
	}

	public boolean isBelowThreshold(double profitOrLoss, double ROI) {
		boolean run = true;
		if (Math.abs(profitOrLoss) > this.stoppingThreshold) {
			run = false;
		}
		if (ROI > this.stoppingThreshold) {
			run = false;
		}
		return run;
	}

	// pause the current strategy
	public void pause() {
		this.status = Status.STOPPED;
		this.run = false;
		// TODO:Implement how to pause
	}

	public Status getStatus() {
		return this.status;
	}

	// Returns entire history of performance of the instance of this strategy
	public LinkedList<Double> getPerformance() {

		return null;
	}

	// Return specific performance at some time t
	public LinkedList<Double> getPerformanceAtTime(Timestamp t) {
		return null;
	}

	public String getId() {
		return id;
	}

	public StrategyType getType() {
		return type;
	}

	public String getStockName() {
		return stockName;
	}

	public List<Double> getStrategyParameters() {
		return strategyParameters;
	}

	public List<Double> getTradeParameters() {
		return tradeParameters;
	}

	public LinkedList<Double> getPrices() {
		return prices;
	}

}
