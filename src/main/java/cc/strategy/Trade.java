package cc.strategy;

import java.io.IOException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cc.model.Model.Decision;
import cc.strategy.Strategy.StrategyType;

@Entity(name = "Trades")
@Table(name = "Trades")
public class Trade {
	@Id
	@GeneratedValue
	@Column(name = "Trade ID", unique = true)
	private String id;

	@Enumerated(EnumType.STRING)
	@Column(name = "Strategy Type", length = 20)
	private StrategyType strategyType;

	@Column(name = "Stock Name")
	private String stockName;

	@Column(name = "Amount of Stocks")
	private Double amount;

	@Enumerated(EnumType.STRING)
	@Column(name = "Decision", length = 20)
	private Decision decision;

	@Column(name = "Price Bought")
	private double price;

	@Column(name = "Timestamp")
	private Timestamp timestamp;

	@Column(name = "Profit or Loss")
	private double profitOrLoss;

	@Column(name = "ROI")
	private double ROI;

	public enum brokerResponse {
		FILLED, PARTIAL_FILLED, REJECTED
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "Broker Response")
	private brokerResponse response;

	public Trade() {
	}

	public Trade(String id, StrategyType strategyName, String stockName, List<Double> tradeParameters,
			Decision decision, Timestamp timestamp) throws Exception {
		this.id = id;
		this.strategyType = strategyName;
		this.stockName = stockName;
		parseParameters(tradeParameters);
		this.decision = decision;
		this.profitOrLoss = 0.0;
		this.ROI = 0.0;
	}

	public brokerResponse getResponse() {
		return this.response;
	}

	public Trade(String responseXml, String correlationID) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(responseXml));

		Document doc = builder.parse(src);
		this.stockName = doc.getElementsByTagName("stock").item(0).getTextContent();
		this.id = correlationID;
		this.amount = Double.parseDouble(doc.getElementsByTagName("size").item(0).getTextContent());
		this.price = Double.parseDouble(doc.getElementsByTagName("price").item(0).getTextContent());
		this.decision = toDecision(doc.getElementsByTagName("buy").item(0).getTextContent());
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(doc.getElementsByTagName("whenAsDate").item(0).getTextContent());
			this.timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) { // this generic but you can control another types of exception
			// look the origin of excption
		}
		System.out.println(this.stockName+":"+this.id+":"+this.amount+":"+this.price+":"+this.decision.toString()+":"+this.timestamp);
	}

	private Decision toDecision(String value) {
		switch (value.toLowerCase()) {
		case "true":
			return Decision.BUY;
		case "false":
			return Decision.SELL;
		default:
			return Decision.DO_NOTHING;
		}
	}

	private void parseParameters(List<Double> tradeParameters) throws Exception {
		// Assume indexed as: amount of stocks, price bought
		if (tradeParameters.size() != 2) {
			throw new Exception("Bad parameters format");
		}

		try {
			this.amount = tradeParameters.get(0);
			this.price = tradeParameters.get(1);

		} catch (Exception e) {
			// throw illegal format exception
			throw new Exception("");
		}
	}

	public String getId() {
		return id;
	}

	public StrategyType getStrategyName() {
		return this.strategyType;
	}

	public String getStockName() {
		return stockName;
	}

	public double getAmount() {
		return this.amount;
	}

	public Decision getDecision() {
		return decision;
	}

	public double getPrice() {
		return price;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public boolean isBuy() {
		return decision == Decision.BUY;
	}

	public void setProfitOrLoss(double profitOrLoss) {
		this.profitOrLoss = profitOrLoss;
	}

	public void setROI(double ROI) {
		this.ROI = ROI;
	}

	public String getStrategyId() {
		return this.id.split("_")[0];
	}
}
